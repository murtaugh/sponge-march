> URLs typically consist of three pieces:
>
>    The scheme identifying the protocol used to access the resource.  
>    The name of the machine hosting the resource.  
>    The name of the resource itself, given as a path.   

Consider for instance [this contemporary link to a presentation about food-borne transmission of viruses on the FTP server of the US CDC](ftp://ftp.cdc.gov/pub/Media/StateHealth/OutbreakNet%20WASH%20Quarterly%20Webinar%20presentations/2019_DFWED_ELC_OBNE_WASH_webinar_041619.pdf).

	1. scheme ftp://
	2. host   ftp.cdc.gov
	3. path   /pub/Media/StateHealth/OutbreakNet%20WASH%20Quarterly%20Webinar%20presentations/
	   file   2019_DFWED_ELC_OBNE_WASH_webinar_041619.pdf

## SPARQL

I realize that my writing here is strewn with acronyms and perhaps not very welcoming to a reader unfamiliar with some of the practices described. However, as Margarita Padilla's writes, there are social truths underlying the specialised discourse of technical protocols.

[JSON-LD and Why I Hate the Semantic Web](https://web.archive.org/web/20141205173750/https://manu.sporny.org/2014/json-ld-origins-2/)

A subtle but quite important feature of using the RDF descriptions for the sponge index is that it allowed us to develop a custom (minimal) vocabulary (itself based on the Dublin Core standard) to describe certain essential relationships of the materials and projects of the site.

![](NWAA-gallery-image-graph.svg)

The key relationships:

* Tags (Documents as)
* Granularity (Part/wholes, Media Fragments)
* Versions

The index is currently implemented using an [RDF store](https://jena.apache.org/) that supports a flexible way of making requests that allows for some important flexibility (for instance allowing editors to link to any language of a SPIP article or to either a gallery page, or the actual image).

```sparql
SELECT DISTINCT ?tagged_all_formats ?mediatype
WHERE {
<https://constantvzw.org/site/-Constant_V,196-.html> (dcterms:hasFormat | ^dcterms:hasFormat | dcterms:isFormatOf | ^dcterms:isFormatOf)* ?tag_all_formats .
?tag_all_formats (dcterms:hasPart | ^ dcterms:isPartOf)* ?tag_and_parts .
?tag_and_parts (aa:isTagof | ^aa:hasTag) ?tagged .
?tagged (dcterms:hasPart | ^ dcterms:isPartOf)* ?tagged_and_parts .
?tagged_and_parts (dcterms:hasFormat | ^dcterms:hasFormat | dcterms:isFormatOf | ^dcterms:isFormatOf)* ?tagged_all_formats .
?tagged_all_formats a aa:mediaitem.
?tagged_all_formats aa:mediatype ?mediatype.
}
```
-----------------


Susan Leigh Star, in describing a feminist methodology specific to information technology, discusses “standards” as one type of “boundary object,” which she describes as:

> [… those] objects which both inhabit several communities of practice and satisfy the information requirements of each of them. Boundary objects are thus objects which are both plastic enough to adapt to local need and common identity across sites. ([source](https://doi.org/10.7551/mitpress/10113.003.0009))

In working critically with standards, we find inspiration in the tradition of work like Hope Olson whose [critical Library Sciences work on classification](https://www.ideals.illinois.edu/bitstream/handle/2142/8210/librarytrendsv47i2f_opt.pdf) speaks of creating "paradoxical spaces" (a term she borrows from feminist geographer Gillian Rose) in which one seeks a kind of "poiesis" or "[alteration] of representation in ways that make boundaries permeable".

> With this understanding, Publishing As Protocol complicates the dual implications of the word ‘protocol’ by purposefully conflating its meanings 1) as the ​accepted or established code of procedure or behaviour in any group, organization, or situation, and 2) as a set of rules governing the exchange or transmission of data between computing devices; in order to consider both definitions equally as protocol that can be not only read and activated, but also written and rewritten.

In considering the call

* protocols coexist with politics...
* technical protocols are not given; but in fact reflect and can 

Working with protocols and standards and at the same time resisting / reversing their politics...

Insistence on keeping the two meanings engaged. (Katherine Hayles techno texts: "mobilizes reflexive loops between its imaginative world and the material apparatus embodying that creation as a physical presence") in other words establishing codes of procedure and behaviour in a group informs one's solidification in rules of exchange of data.
read / write

Working with forgotten / failed formats, tools, workflows, writing practices as a way of sustaining the activities of a group/organization... and that resists a techno-determinism in seeing how these protocols themselves can best be deployed in response to the (already vibrant) practices of those  individuals/groups/organisations.

Passivity of "go with the flow"... work with the flow / co-habitate.

semantic web ... values in opposition to a web of solidarity...
Embracing multiplicity (as in multiple editing environments) to support different kinds of temporality / (situated) models of trust / prioritizing of resources and matching these with an appreciation of the life cycle of projects and events. (matching different forms and priorities of infrastructures with the life cycle of projects and events)

non-neutrality of the cycle -- each needs to respond to the other...<>!>
(tools <-> practice)

Margarita Padilla: Social truths engendered by technology. "communities are generous"...(speaking about different ways to participate in a community)... takes "good intentions" (politics)
"technology is to culture what the body is to life" (antonio rodriqeuz de las Heras) "free software is the software that practics and defends the values of the community"

link to the sponge ... boundaries permeable ...
standards as boundary objects

GO SPONGE!!!!! POROUS ...

(from femke)
> It seems these particular protocols are crucial for the website of Constant because the Sponge ecosystem is all about “aggregation”: allowing materials to be interlinked, presented in relation to each other – hence the vocabulary of weaving and wefting. Sponge, webrings, wefting and weaving … but also the semantic web and classification are each protocols for aggregation, though with different politics?
> 

* CONGRESSIVE! (shout out to Eugenia Cheng)


* expressing locally relevant vocabularies that create alternative points of convergance through aggregation.

- [ ] Frequent frustration between the imagination triggered by a language for "" (RDF QUOTE ?!) and the frustration of the "reality" of often how such standards are interpreted and put in practice.
